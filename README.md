# Unity-Git-setup

The .gitignore and .gitattributes files can be used for Unity projects using [Git LFS](https://www.atlassian.com/git/tutorials/git-lfs?_ga=2.126827277.1472899469.1539009679-2117100450.1520950722#installing-git-lfs) and Unity's default Smart Merge, aka UnityYAMLMerge.

The following, after updating the path to UnityYAMLMerge, should be added to your git config file as well:

```
[merge]
    tool = unityyamlmerge

    [mergetool "unityyamlmerge"]
    trustExitCode = false
    cmd = '<path to UnityYAMLMerge>' merge -p "$BASE" "$REMOTE" "$LOCAL" "$MERGED"
```

The path to UnityYAMLMerge, will be:

```<Unity installation directory>/Editor/Data/Tools/UnityYAMLMerge``` ...on Linux,\

```/Applications/Unity/Unity.app/Contents/Tools/UnityYAMLMerge``` ...on OSX, and\

```C:\Program Files\Unity\Editor\Data\Tools\UnityYAMLMerge.exe``` or ```C:\Program Files (x86)\Unity\Editor\Data\Tools\UnityYAMLMerge.exe``` ...on Windows.


If deciding to use another merge tool, changes in the config as well as in the .gitattributes file should be made.